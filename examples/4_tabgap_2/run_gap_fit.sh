#!/bin/bash

gap_fit atoms_filename=db_train.xyz \
do_copy_atoms_file=False \
gap_file=gap_example.xml \
rnd_seed=999 \
core_param_file=pairpot.xml \
core_ip_args={IP Glue} \
default_sigma={0.002 0.04 0.1 0.0} \
config_type_sigma={isolated_atom:0.0001:0.01:0.01:0.0:dimer:0.1:0.8:0.1:0.0:liquid:0.01:0.5:1.0:0.0:short_range:0.01:0.5:1.0:0.0} \
gap={distance_2b \
        cutoff=5.0 \
        covariance_type=ard_se \
        delta=1.0 \
        theta_uniform=1.0 \
        sparse_method=uniform \
        n_sparse=20 \
        print_sparse_index=sparse_indices_2b.out \
     : angle_3b \
        cutoff=4.1 \
        cutoff_transition_width=0.7 \
        n_sparse=300 \
        covariance_type=ard_se \
        delta=0.1 \
        theta_uniform=1.0 \
        sparse_method=uniform \
        print_sparse_index=sparse_indices_3b.out \
        add_species=T \
    }

# remove unnecessary files
rm *.idx
