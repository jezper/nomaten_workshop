import numpy as np
import ase.io
from quippy.potential import Potential
import matplotlib.pyplot as plt


def calc_errors(gap_file, xyz_file, plot=False):
    gap = Potential(param_filename=gap_file)
    all_frames = ase.io.read(xyz_file, index=':')

    energies_ref, forces_ref = {}, {}
    energies_gap, forces_gap = {}, {}
    e_all, f_all = [], []

    for i, atoms in enumerate(all_frames):

        # separate by config_type
        config = atoms.info['config_type']
        if config not in energies_ref.keys():
            energies_ref[config] = []
            energies_gap[config] = []
            forces_ref[config] = []
            forces_gap[config] = []

        # get 'true' reference energy/forces
        try:
            E = atoms.info['free_energy'] / len(atoms)
        except KeyError:
            E = atoms.info['energy'] / len(atoms)
        # sometimes it is 'force' (quip default), sometimes 'forces' (ASE default)
        try:
            F = list(np.ndarray.flatten(atoms.arrays['forces']))
        except KeyError:
            F = list(np.ndarray.flatten(atoms.arrays['force']))
        energies_ref[config].append(E)
        forces_ref[config].extend(F)
        e_all.append(E)
        f_all.extend(F)

        # compute GAP-predicted energy/forces
        atoms.calc = gap
        E = atoms.get_potential_energy() / len(atoms)
        F = list(np.ndarray.flatten(atoms.calc.results['forces']))
        energies_gap[config].append(E)
        forces_gap[config].extend(F)
        e_all.append(E)
        f_all.extend(F)

    # calculate root-mean-square-errors for each config_type
    RMSE_E, RMSE_F = {}, {}
    print(f'\nFile {xyz_file}')
    for config in energies_ref.keys():
        e_ref = np.asarray(energies_ref[config])
        f_ref = np.asarray(forces_ref[config])
        e_gap = np.asarray(energies_gap[config])
        f_gap = np.asarray(forces_gap[config])
        RMSE_E[config] = np.sqrt(np.mean((e_gap - e_ref)**2)) * 1e3  # in meV/atom
        RMSE_F[config] = np.sqrt(np.mean((f_gap - f_ref)**2))  # in eV/Å
        print(f'{config} Energy RMSE: {round(RMSE_E[config], 3)} meV/atom')
        print(f'{config} Force  RMSE: {round(RMSE_F[config], 3)} eV/Å')

    if plot:
        fig, ax = plt.subplots(ncols=2, figsize=(10, 5))
        for config in energies_ref.keys():
            ax[0].plot(energies_ref[config], energies_gap[config], 'o', mfc='None',
                       label=f'{config} RMSE: {round(RMSE_E[config], 3)} meV/atom')
            ax[1].plot(forces_ref[config], forces_gap[config], 'o', mfc='None',
                       label=f'{config} RMSE: {round(RMSE_F[config], 3)} eV/Å')
        # plot lines of perfect agreement
        elim = (min(e_all) - 0.05, max(e_all) + 0.05)
        flim = (min(f_all) - .5, max(f_all) + .5)
        ax[0].plot(elim, elim, 'k-')
        ax[1].plot(flim, flim, 'k-')
        ax[0].set_xlim(elim)
        ax[0].set_ylim(elim)
        ax[1].set_xlim(flim)
        ax[1].set_ylim(flim)
        ax[0].legend()
        ax[1].legend()
        plt.show()

    return RMSE_E, RMSE_F


E, F = calc_errors('gap_example.xml', 'db_train.xyz', plot=True)
E, F = calc_errors('gap_example.xml', 'db_test.xyz', plot=True)
