#!/bin/bash

gap_fit atoms_filename=db_train.xyz \
do_copy_atoms_file=False \
gap_file=gap_example.xml \
rnd_seed=999 \
default_sigma={0.002 0.04 0.1 0.0} \
config_type_sigma={isolated_atom:0.0001:0.01:0.01:0.0:fcc_rattled:0.002:0.04:0.1:0.0} \
energy_parameter_name=free_energy \
force_parameter_name=forces \
gap={distance_2b \
        cutoff=5.0 \
        covariance_type=ard_se \
        delta=1.0 \
        theta_uniform=1.0 \
        sparse_method=uniform \
        n_sparse=20 \
        print_sparse_index=sparse_indices_2b.out \
    }
#     : angle_3b \
#        cutoff=4.1 \
#        cutoff_transition_width=0.7 \
#        n_sparse=100 \
#        covariance_type=ard_se \
#        delta=0.1 \
#        theta_uniform=1.0 \
#        sparse_method=uniform \
#        print_sparse_index=sparse_indices_3b.out \
#        add_species=T \
#    : soap \
#        cutoff=5.0 \
#        cutoff_transition_width=1.0 \
#        l_max=2 \
#        n_max=4 \
#        delta=0.2 \
#        atom_sigma=0.5 \
#        zeta=4 \
#        n_sparse=100 \
#        sparse_method=cur_points \
#        covariance_type=dot_product \
#        print_sparse_index=sparse_indices_soap.out \
#    }

# remove unnecessary files
rm *.idx
