#!/usr/bin/env python3
import numpy as np
import argparse
import ase.io
from quippy.potential import Potential
import matplotlib.pyplot as plt
from ase.calculators.emt import EMT

gap_xml = 'gap_example.xml'
gap = Potential(param_filename=gap_xml)

atoms = ase.build.bulk('Cu', cubic=True)
orig_cell = atoms.get_cell()
strains = np.linspace(0.9, 1.1, 20)
cells = [orig_cell * s for s in strains]

energies_ref, energies_gap, volumes = [], [], []
for cell in cells:
    at = atoms.copy()
    at.set_cell(cell, scale_atoms=True)
    vol = at.get_volume()

    # EMT reference energy
    at.calc = EMT()
    E_ref = at.get_potential_energy()

    # GAP energy
    at.calc = gap
    E_gap = at.get_potential_energy()

    energies_ref.append(E_ref/len(atoms))
    energies_gap.append(E_gap/len(atoms))
    volumes.append(vol/len(atoms))


# print(volumes)
# print(energies_ref)
# print(energies_gap)

plt.plot(volumes, energies_ref, 'k.--', label='EMT')
plt.plot(volumes, energies_gap, 'C3o-', label='GAP')
plt.legend()
plt.xlabel('volume per atom')
plt.ylabel('energy per atom')
plt.show()
