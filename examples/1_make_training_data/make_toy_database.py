import numpy as np
from ase import Atoms
from ase.calculators.emt import EMT
import ase.build.bulk

# Create some example structures for GAP-training with toy energies & forces
# from ASE's EMT (effective medium theory) potential


# we always need the energy of the isolated atom in vacuum as the reference energy
def make_isolated_atom(element='Cu'):
    atoms = Atoms('Cu', positions=[[0, 0, 0]], cell=[50, 50, 50])
    atoms.calc = EMT()
    E = atoms.get_potential_energy()
    # Separate different types of structures with a 'config_type' tag,
    # this allows to set different weigths in GAP-fitting.
    atoms.info['config_type'] = 'isolated_atom'
    return [atoms]


# some small FCC boxes with the atoms randomly displaced (rattled) slightly
def make_rattled_fcc(N=30, element='Cu'):
    atoms = ase.build.bulk(element, cubic=True).repeat((2, 2, 2))
    db = []
    for i in range(N):
        at = atoms.copy()
        at.rattle(stdev=0.05, seed=i)
        at.calc = EMT()
        E = at.get_potential_energy()  # computes energies & forces
        S = at.get_stress(voigt=False)
        # gap_fit wants the virial as virial = -stress_from_ase * volume  !!
        at.info['virial'] = -S * at.get_volume()
        del at.calc.results['stress']
        at.info['config_type'] = 'fcc_rattled'
        db.append(at)
    return db


# some small FCC boxes with a vacancy
def make_fcc_vacancy(N=15, element='Cu'):
    atoms = ase.build.bulk(element, cubic=True).repeat((2, 2, 2))
    del atoms[0]
    db = []
    for i in range(N):
        at = atoms.copy()
        at.rattle(stdev=0.05, seed=i*100)
        at.calc = EMT()
        E = at.get_potential_energy()  # computes energies & forces
        S = at.get_stress(voigt=False)
        # gap_fit wants the virial as virial = -stress_from_ase * volume  !!
        at.info['virial'] = -S * at.get_volume()
        del at.calc.results['stress']
        at.info['config_type'] = 'fcc_vacancy'
        db.append(at)
    return db


## optionally: write your own function(s) to create more training structures!
#def make_...()


### Gather structures into a train and test database and save as .xyz files
db_train = []
db_train.extend(make_isolated_atom())
fcc_rattled = make_rattled_fcc()
fcc_vacancy = make_fcc_vacancy()
db_train.extend(fcc_rattled[:20])
db_train.extend(fcc_vacancy[:10])

db_test = fcc_rattled[20:]
db_test.extend(fcc_vacancy[10:])

ase.io.write('db_train.xyz', db_train)
ase.io.write('db_test.xyz', db_test)
