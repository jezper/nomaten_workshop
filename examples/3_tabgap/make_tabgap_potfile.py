from tabulate.tabgap import tabGAP

# set up tabgap class and grid sizes (will parse needed GAP information from .xml file)
tabgap = tabGAP('../2_gap_fit/gap_example.xml',
                n2b=1000,
                n3b=(50, 50, 50),
                verbose=True)

# compute energies for all grid points by running QUIP in the background.
# For large 3b grids, this may take several minutes per element triplet,
# but it is trivially parallelised, so use multiple cores.
tabgap.compute_energies(ncores=3)

# write the actual potential file(s) to be used in LAMMPS
tabgap.write_potential_files(verbose=True)
